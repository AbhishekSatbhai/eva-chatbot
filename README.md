# eva-chatbot

EVA virtual assistant is a software agent that can perform tasks or services for an individual. Sometimes the term "chatbot" is used to refer to virtual assistants generally or specifically those accessed by online chat.

## Screenshots
![Screenhots](eva.gif)

## Problem Statement
* CONFUSING CAMPUS ENVIRONMENT FOR COLLEGE FRESHERS/GUESTS
* ADMISSION PROCESS IS COMPLEX
* LACK OF INFORMATION (NOT ALL THE INFORMATION AVAILABLE ON COLLEGE WEBSITE)


## Solution 
* EASY TO USE ENTITY
* ONE STOP FOR ALL THE INFORMATION
* INTERACTIVE HELP
* PICTORIAL FORM OF     REPRESENTATION

## Description
A virtual assistant is a software agent that can perform tasks or services for an individual. Sometimes the term "chatbot" is used to refer to virtual assistants generally or specifically those accessed by online chat.

E.G. Siri, Google Assistant

## Tech Stack
* Java
* Xml
* Php
* SQL

## Interested ? Need more Details ?
Visit [PPT](https://gitlab.com/AbhishekSatbhai/eva-chatbot/-/blob/main/AI%20Bot%20PPT_final.pptx) for this information.


## Authors and acknowledgment
Abhishek Satbhai
Chinmayi Shaligram
Rohit Tulsiani
Pranva Chawak


## Project status
Project Completed, development is stopped completely. If someone wants to choose to fork or volunteer to keep going you are welcome :)
