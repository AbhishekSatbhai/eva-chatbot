//only for the purpose of the font
package com.example.asus.aibot.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.asus.aibot.R;

public class Class_MyTextView extends android.support.v7.widget.AppCompatTextView{
	
	public Class_MyTextView(Context context) {
		super(context);
		init(null);
	}
	
	public Class_MyTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public Class_MyTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	private void init(AttributeSet attrs) {
		
		if (attrs!=null) {
			 TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.Class_MyTextView);
			 String fontName = a.getString(R.styleable.Class_MyTextView_fontName);
			 if (fontName!=null) {
				 Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/"+fontName);
				 setTypeface(myTypeface);
			 }
			 a.recycle();
		}
	}

}
