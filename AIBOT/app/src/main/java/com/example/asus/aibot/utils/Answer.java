package com.example.asus.aibot.utils;


public class Answer {

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAnswer() {
        return answer;
    }

    public String getAttendance() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    private String status;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    private String question;
    private String answer;

    public String getAnswerImage() {
        return answerImage;
    }

    public void setAnswerImage(String answerImage) {
        this.answerImage = answerImage;
    }

    private String answerImage;


}
