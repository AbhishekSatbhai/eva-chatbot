package com.example.asus.aibot.utils;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RetrofitAPI {

    @FormUrlEncoded
    @POST("mobile_get_answer.php")
    Call<Answer> getAnswer(@Field("question") String params);

}
/*dont forget to add dependency for retrofit api
    it is in "build.gradle"  */