package com.example.asus.aibot;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

//importing stuff we need
import com.example.asus.aibot.utils.Answer;                     //answer class
import com.example.asus.aibot.utils.Class_ConnectionDetector;   //connection class
import com.example.asus.aibot.utils.RetrofitAPI;                //Retrofit API
import com.squareup.picasso.Picasso;                            //speak out

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

//importing retrofit
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//for sending offline msg
import android.Manifest;
import android.content.pm.PackageManager;
import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;

            //MAIN CLASS
public class Act_Answer extends AppCompatActivity implements View.OnClickListener, TextToSpeech.OnInitListener             //implements View.OnClickListener, TextToSpeech.OnInitListener
{
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0 ;                       //offline sms

    //declare all the views used for designed which are going to be used in code
    TextView tv_question, tv_answer;
    EditText et_question, et_mobile,et_erp;
    ImageView img_listenAudio ,img_send, img_answer, img_whatsapp ,img_sms,img_atm,img_hostel,img_fastfood,img_emergency,img_mess,img_medical,img_senderp;                        //img_answer =if there is image in answer  --sms
    String strQuestion = "", strAnswer = "", strAnswerImage = "",strQuestion_user;               //strings for storing que , ans , image
    TextToSpeech textToSpeech;                                                  //text to speech object
    RelativeLayout rl_image;
    ProgressBar progressView;
    ImageButton btn_fab ,btn_map,btn_erp;                                              //google map button
    TableRow tr_share,tr_map,tr_erp;
    String shareText,strMobile,strprn,strattendance;                                                 //sms share


    boolean isOpen = false;                                                     //for whats app slider part -at first it is hidden so isopen = false
    boolean isOpen2= false;
    boolean isOpen3= true;

    private String mobile_number = "^[6789]\\d{9}$";                             //10 digit mobile number for sharing throught wapp (Regular expression used)

    //Retrofit API

    Class_ConnectionDetector cd;                                                //creating object of class_connection for connectivity
    Retrofit retrofit = new Retrofit.Builder()                                  //using retrofit to create an http request and also process the http response
            .baseUrl(Class_Global.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    RetrofitAPI apiService = retrofit.create(RetrofitAPI.class);

    public static final int REQ_CODE_SPEECH_INPUT = 1;                          //google speech to text


    @Override
    protected void onCreate(Bundle savedInstanceState)                          //oncreate activity
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.answer);                                 //activity_answer.xml  will be created
        cd = new Class_ConnectionDetector(this);
        init();                                                                 //function called
    }

    // Initiate all the views
    public void init()
    {
        //assigning ID's
        tv_question = (TextView) findViewById(R.id.tv_question);                //all TextView
        tv_answer = (TextView) findViewById(R.id.tv_answer);

        et_question = (EditText) findViewById(R.id.et_question);                //all EditView
        et_mobile = (EditText) findViewById(R.id.et_mobile);
        et_erp = (EditText) findViewById(R.id.et_erp);

        img_listenAudio = (ImageView) findViewById(R.id.img_listenAudio);       //all img
        img_send= (ImageView) findViewById(R.id.img_send);
        img_answer = (ImageView) findViewById(R.id.img_answer);
        img_whatsapp = (ImageView) findViewById(R.id.img_whatsapp);
        img_sms = (ImageView) findViewById(R.id.img_sms);                       //sms msg
        img_senderp= (ImageView) findViewById(R.id.img_senderp);

        rl_image = (RelativeLayout) findViewById(R.id.rl_image);                //others
        progressView = (ProgressBar) findViewById(R.id.progressView);

        btn_fab = (ImageButton) findViewById(R.id.btn_fab);                     //all image buttons
        btn_map = (ImageButton) findViewById(R.id.btn_map);
        btn_erp = (ImageButton) findViewById(R.id.btn_erp);

        tr_share = (TableRow) findViewById(R.id.tr_share);                      //all table rows
        tr_map=(TableRow) findViewById(R.id.tr_map);
        tr_erp=(TableRow) findViewById(R.id.tr_erp);

                                                                                //all image view
        img_atm = (ImageView) findViewById(R.id.img_atm);                      //map details
        img_hostel = (ImageView) findViewById(R.id.img_hostel);
        img_emergency = (ImageView) findViewById(R.id.img_emergency);
        img_fastfood = (ImageView) findViewById(R.id.img_fastfood);
        img_mess = (ImageView) findViewById(R.id.img_mess);
        img_medical= (ImageView) findViewById(R.id.img_medical);

        //setting up the onclick event

        img_whatsapp.setOnClickListener(this);                                  //to share
        img_sms.setOnClickListener(this);                                       //sms
        img_send.setOnClickListener(this);                                      //send button

        btn_fab.setOnClickListener(this);                                       //sharing tab (oranage colour)
        btn_map.setOnClickListener(this);                                       //GOOGLE MAP BUTTON
        btn_erp.setOnClickListener(this);

        img_listenAudio.setOnClickListener(this);                               //mic (speech to text)
        img_atm.setOnClickListener(this);                                       //map details
        img_hostel.setOnClickListener(this);
        img_emergency.setOnClickListener(this);
        img_fastfood.setOnClickListener(this);
        img_mess.setOnClickListener(this);
        img_medical.setOnClickListener(this);
        img_senderp.setOnClickListener(this);

      //  img_answer.setOnClickListener(this);                                     //```````````````````````````````````````````````````````````````

        Bundle extras = getIntent().getExtras();                                //import the question ,answer ,answeriamage form the Act_quetion
        if (extras != null)
        {
            strQuestion = extras.getString("question", "");
            strAnswer = extras.getString("answer", "");
            strAnswerImage = extras.getString("answerImage", "");
        }


        tv_question.setText(strQuestion);                                       //setText used to display inside TextView  //displaying question
        tv_answer.setText(strAnswer);                                           //displaying answer
        showDownloadImage(strAnswerImage);                                      //displaying image
        textToSpeech = new TextToSpeech(this, this);                            //textToSpeech initiated
        speakOut();                                                             //speak out
    }


    //-----------send question to the server ---
    public void getAnswer() {
        final ProgressDialog dialog = new ProgressDialog(Act_Answer.this);
        dialog.setTitle(R.string.app_name);                                              //when user asks for question : we will display dialog box  saying "title ,searching for your question"
        dialog.setMessage("Searching for your question..!!");
        dialog.setCancelable(false);
        dialog.show();

    HashMap<String, String> params = new HashMap<>();                           //send the question  to the server
        params.put("question", strQuestion);

        Call<Answer> call = apiService.getAnswer(strQuestion);                  //send tto question  to the server
        call.enqueue(new Callback<Answer>() {
            @Override
            public void onResponse(Call<Answer> call, Response<Answer> response) {
                dialog.dismiss();
                parseResponse(response.body());                                 //calling function response //this function will receive the response from mobile_php file in the server
            }

            @Override
            public void onFailure(Call<Answer> call, Throwable t) {             //onfailure activity
                dialog.dismiss();
                Toast.makeText(Act_Answer.this, "Internal Server Error.!\nPlease Try Later.!", Toast.LENGTH_SHORT).show();
            }
        });
    }
//~~~~~~~ Received  response got from server- checking--------------
    private void parseResponse(Answer response) {
        try {
            final String status = response.getStatus();
            strAnswer = response.getAnswer();
            strAnswerImage = response.getAnswerImage();
            strQuestion = response.getQuestion();

            if (status.equals("success"))                                       //~~~~~~~ If answer found
            {
                et_question.setText("");                                        //make type box empty
                tv_question.setText(strQuestion_user);                               //diaplay question in tv_question  `````````making changes strQuestion replced by strQuestion_user`````````
                tv_answer.setText(strAnswer);                                   //displaying answer in tv_answer
                showDownloadImage(strAnswerImage);                              //displaying img
                if (textToSpeech != null) {
                    textToSpeech.stop();
                }
                speakOut();                                                     //read out

            } else if (status.equals("failure"))                                //~~~~~~~ If answer not found
            {
                Toast.makeText(Act_Answer.this, strAnswer, Toast.LENGTH_SHORT).show();      //alert box Toast.makeTest(name of activity  to appear,string to display)
                tv_question.setText(strQuestion);
                tv_answer.setText(strAnswer);
                rl_image.setVisibility(View.GONE);
                img_answer.setVisibility(View.GONE);
                progressView.setVisibility(View.GONE);
            }
            }//end of try
        catch (Exception e)
        {
            Toast.makeText(Act_Answer.this, "Internal Server Error..\nPlease Try Again.!", Toast.LENGTH_SHORT).show();
        }
    }
    private void promptSpeechInput()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try
        {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        }
        catch (ActivityNotFoundException a)
        {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        //~~~~~ Get text from google api
        switch (requestCode)
        {
            case REQ_CODE_SPEECH_INPUT:
            {
                if (resultCode == RESULT_OK && null != data)
                {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    et_question.setText(result.get(0));
                    img_send.performClick();                                                        //send button click
                }
                break;
            }
        }
    }

    //onClick event
    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.img_listenAudio)                                                     // On mic click
        {
            promptSpeechInput();
        }
        else if (view.getId() == R.id.img_send)                                                      // On ask me click
        {
            strQuestion = et_question.getText().toString().trim();
            strQuestion_user = et_question.getText().toString().trim();
            if (strQuestion.trim().length() == 0)                       //validating length
            {
                Toast.makeText(this, "Please enter your question..!", Toast.LENGTH_SHORT).show();           //toast is small pop up which " comes -displays - goes "
            }
            else if (!cd.isConnectingToInternet())                      //checking net connection
            {
                Toast.makeText(this, "No internet connection..!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                getAnswer();                     //~~~ Call webservice from php controller MobileApp
            }
        }
        else if (view.getId() == R.id.btn_fab)                                                       // On share button  click    ---------------------???????
        {
            if (isOpen)
            {
                isOpen = false;                         //not open
                tr_share.setVisibility(View.GONE);
                Animation RightSwipe = AnimationUtils.loadAnimation(Act_Answer.this, R.anim.right);          //decalring animation
                tr_share.startAnimation(RightSwipe);                                                         //applying animation
            }
            else
            {
                isOpen = true;                          //open
                tr_share.setVisibility(View.VISIBLE);
                Animation leftSwipe = AnimationUtils.loadAnimation(Act_Answer.this, R.anim.left);            //decalring animation
                tr_share.startAnimation(leftSwipe);                                                           //applying animation
            }
        }
        else if (view.getId() == R.id.btn_map)                                                       // On google map button  click
        {
            if (isOpen2)
            {
                isOpen2 = false;                         //not open
                tr_map.setVisibility(View.GONE);
                Animation RightSwipe = AnimationUtils.loadAnimation(Act_Answer.this, R.anim.right);          //decalring animation
                tr_map.startAnimation(RightSwipe);                                                         //applying animation
            }
            else
            {
                isOpen2 = true;                          //open
                tr_map.setVisibility(View.VISIBLE);
                Animation leftSwipe = AnimationUtils.loadAnimation(Act_Answer.this, R.anim.left);            //decalring animation
                tr_map.startAnimation(leftSwipe);                                                           //applying animation
            }

        }
        else if (view.getId() == R.id.btn_erp)                                                       // On ERP button  click    ---------------------???????
        {
            if (isOpen3)
            {
                isOpen3 = false;                         //not open
                tr_erp.setVisibility(View.GONE);
                Animation rightSwipe = AnimationUtils.loadAnimation(Act_Answer.this, R.anim.left);          //decalring animation
                tr_erp.startAnimation(rightSwipe);                                                         //applying animation
            }
            else
            {
                isOpen3 = true;                          //open
                tr_erp.setVisibility(View.VISIBLE);
                Animation leftSwipe = AnimationUtils.loadAnimation(Act_Answer.this, R.anim.right);            //decalring animation
                tr_erp.startAnimation(leftSwipe);                                                           //applying animation
            }
        }
        else if (view.getId() == R.id.img_whatsapp)                                                   // On share on whatsapp click
        {
            String strMobile = et_mobile.getText().toString().trim();
            if (strMobile.matches(mobile_number))                    //validating mobile number
            {
                String shareText = "Question : \n" + strQuestion + "\n\n\n" + "Answer : \n" + strAnswer;
                try
                {
                    Intent sendIntent = new Intent("android.intent.action.MAIN");
                    //sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
                    sendIntent.putExtra("jid", "91" + strMobile + "@s.whatsapp.net");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setPackage("com.whatsapp");
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }
                catch (Exception e)
                {
                    Toast.makeText(this, "Unable to share your message..", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(this, "Please enter valid mobile number..!", Toast.LENGTH_SHORT).show();
            }
        }
        else if (view.getId() == R.id.img_sms)                      //sns service on click btn_sms
        {
            strMobile = et_mobile.getText().toString().trim();
            if (strMobile.matches(mobile_number))                    //validating mobile number
            {
                shareText = "Question : \n" + strQuestion + "\n\n\n" + "Answer : \n" + strAnswer;
                try
                {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.SEND_SMS)
                            != PackageManager.PERMISSION_GRANTED)
                    {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                Manifest.permission.SEND_SMS))
                        {
                        } else
                        {
                            ActivityCompat.requestPermissions(this,
                                    new String[]{Manifest.permission.SEND_SMS},
                                    MY_PERMISSIONS_REQUEST_SEND_SMS);
                        }
                    }

                }
                catch (Exception e)
                {
                    Toast.makeText(this, "Unable to share your message..", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(this, "Please enter valid mobile number..!", Toast.LENGTH_SHORT).show();
            }
            //--------------SMS--------------

        }
        else if (view.getId() == R.id.img_senderp)                      //sns service on click btn_sms
        {
            strprn = et_erp.getText().toString().trim();
            if (strprn.trim().length() == 0)                       //validating length
            {
                Toast.makeText(this, "Please enter your PRN number..!", Toast.LENGTH_SHORT).show();           //toast is small pop up which " comes -displays - goes "
            }
            else if (!cd.isConnectingToInternet())                      //checking net connection
            {
                Toast.makeText(this, "No internet connection..!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                if(strprn.equals("S0115011321"))
                {
                    Toast.makeText(this, "your attendance is : 77%", Toast.LENGTH_SHORT).show();           //
                }
                else if (strprn.equals("S0115011322"))
                {
                    Toast.makeText(this, "your attendance is : 65%", Toast.LENGTH_SHORT).show();
                }
                else if (strprn.equals("S0115011323"))
                {
                    Toast.makeText(this, "your attendance is : 80%", Toast.LENGTH_SHORT).show();
                }
                else if (strprn.equals("S0115011324"))
                {
                    Toast.makeText(this, "your attendance is : 75%", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(this, "Please check your PRN number !", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if (view.getId() == R.id.img_atm)                                                        // On img_atm click map
        {
            String flag="atm";
            Intent intent = new Intent(Act_Answer.this, MapsActivity.class);            //this will send the flag from Act_Activity to MapsActivity
            intent.putExtra("flag", flag);                                      //exporting the flag for the map
            startActivity(intent);                                                      //it will start the next activity i.e after Act_Answer -> MapsActivity

           // Intent i = new Intent(getApplicationContext(),MapsActivity.class);                  //shift from one activity to other ()from anser to map
           // startActivity(i);
        }
        else if (view.getId() == R.id.img_fastfood)                                                   // On img_fastfood click map
        {
            String flag="fastfood";
            Intent intent = new Intent(Act_Answer.this, MapsActivity.class);
            intent.putExtra("flag", flag);
            startActivity(intent);

        }
        else if (view.getId() == R.id.img_emergency)                                                  // On img_emergency click map
        {
            String flag="emergency";
            Intent intent = new Intent(Act_Answer.this, MapsActivity.class);
            intent.putExtra("flag", flag);
            startActivity(intent);
        }
        else if (view.getId() == R.id.img_hostel)                                                     // On img_hotel click map
        {
            String flag="hostel";
            Intent intent = new Intent(Act_Answer.this, MapsActivity.class);
            intent.putExtra("flag", flag);
            startActivity(intent);
        }
        else if (view.getId() == R.id.img_mess)                                                       // On img_mess click map
        {
            String flag="mess";
            Intent intent = new Intent(Act_Answer.this, MapsActivity.class);
            intent.putExtra("flag", flag);
            startActivity(intent);
        }
        else if (view.getId() == R.id.img_medical)                                                       // On img_trial click map
        {
            String flag = "medical";
            Intent intent = new Intent(Act_Answer.this, MapsActivity.class);
            intent.putExtra("flag", flag);
            startActivity(intent);
        }
    }

//sms service
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(strMobile, null, shareText, null, null);
                    Toast.makeText(getApplicationContext(), "SMS sent.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }
    }
//~~~~~~~~~~ On activity destroy,  stop the TextToSpeech
    @Override
    public void onDestroy() {
        // Don't forget to shutdown textToSpeech!
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }
    //~~~~~~~~~~ On activity stop,  stop the TextToSpeech
    @Override
    public void onStop() {
        // Don't forget to shutdown textToSpeech!
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onStop();
    }
    //~~~~~~~~~~ On TextToSpeech start
    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = textToSpeech.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    //~~~~~ Speak answer
    private void speakOut() {
        String text = tv_answer.getText().toString();
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }
    // ~~~~~~~~~~ Download image if answer having image
    public void showDownloadImage(String strAnswerImage) {
        try {
            if (strAnswerImage.trim().length() != 0) {
                rl_image.setVisibility(View.VISIBLE);
                progressView.setVisibility(View.VISIBLE);
                img_answer.setVisibility(View.VISIBLE);
                String string_image = Class_Global.BASE_IMAGE_URL + strAnswerImage;
                Picasso.with(Act_Answer.this)
                        .load(string_image)
                        //.placeholder(img_receive.getDrawable())
                        // .transform(new RoundedTransformation(15, 0))
                        // .fit()
                        .into(img_answer, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                progressView.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                img_answer.setVisibility(View.GONE);
                                progressView.setVisibility(View.GONE);
                                rl_image.setVisibility(View.GONE);

                            }
                        });
            } else {
                img_answer.setVisibility(View.GONE);
                progressView.setVisibility(View.GONE);
                rl_image.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            img_answer.setVisibility(View.GONE);
            progressView.setVisibility(View.GONE);
            rl_image.setVisibility(View.GONE);
        }
    }
    //~~~~ On back button pressed
    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
