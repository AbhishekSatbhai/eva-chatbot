package com.example.asus.aibot;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;

import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.aibot.utils.Answer;
import com.example.asus.aibot.utils.Class_ConnectionDetector;
import com.example.asus.aibot.utils.RetrofitAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Act_Question extends AppCompatActivity implements View.OnClickListener
{

    EditText et_question;
    ImageView img_listenAudio,img_send;
    String strQuestion,strQuestion_user;
    TableRow tr_dialoginstitute,tr_dialogmap,tr_dialogprof;         //for animation
    TextView tv_something1;

    boolean doubleBackToExitPressedOnce = false;
    boolean isOpen = false;

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Class_Global.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    RetrofitAPI apiService = retrofit.create(RetrofitAPI.class);


    public static final int REQ_CODE_SPEECH_INPUT = 1;

    Class_ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cd = new Class_ConnectionDetector(this);
        init();

        //animation
            tv_something1.setVisibility(View.VISIBLE);
            tr_dialogmap.setVisibility(View.VISIBLE);
            tr_dialoginstitute.setVisibility(View.VISIBLE);
            tr_dialogprof.setVisibility(View.VISIBLE);

            Animation LeftSwipe = AnimationUtils.loadAnimation(Act_Question.this, R.anim.left);          //decalring animation
            tv_something1.startAnimation(LeftSwipe);
            tr_dialogmap.startAnimation(LeftSwipe);                                                       //````````````````````````delay wanted   ???????????1111111111111
            tr_dialogprof.startAnimation(LeftSwipe);
            tr_dialoginstitute.startAnimation(LeftSwipe);
    }

    // Initiate views
    public void init() {
        et_question = (EditText) findViewById(R.id.et_question);
        img_listenAudio = (ImageView) findViewById(R.id.img_listenAudio);
        img_send = (ImageView) findViewById(R.id.img_send);

        //for front animation
        tv_something1= (TextView) findViewById(R.id.tv_somethings1);
        tr_dialogmap= (TableRow) findViewById(R.id.tr_dialogmap);
        tr_dialogprof= (TableRow) findViewById(R.id.tr_dialogprof);
        tr_dialoginstitute= (TableRow) findViewById(R.id.tr_dialoginstitue);

        img_send.setOnClickListener(this);
        img_listenAudio.setOnClickListener(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_listenAudio) {
            promptSpeechInput();
        } else if (view.getId() == R.id.img_send) {
            strQuestion = et_question.getText().toString().trim();
            strQuestion_user=et_question.getText().toString().trim();               //storing users question in strQuestion_user
            if (strQuestion.trim().length() == 0) {
                Toast.makeText(this, "Please enter your question..!", Toast.LENGTH_SHORT).show();
            } else if (!cd.isConnectingToInternet()) {
                Toast.makeText(this, "No internet connection..!", Toast.LENGTH_SHORT).show();
            } else {
                getAnswer();
            }
        }
    }


    //-----------Get answer from server using retrofit api --- //send questrion to the server
    public void getAnswer() {
        final ProgressDialog dialog = new ProgressDialog(Act_Question.this);
        dialog.setTitle(R.string.app_name);
        dialog.setMessage("Searching for your question..!!");
        dialog.setCancelable(false);
        dialog.show();

        HashMap<String, String> params = new HashMap<>();
        params.put("question", strQuestion);

        Call<Answer> call = apiService.getAnswer(strQuestion);
        call.enqueue(new Callback<Answer>() {
            @Override
            public void onResponse(Call<Answer> call, Response<Answer> response) {
                dialog.dismiss();
                parseResponse(response.body());
            }

            @Override
            public void onFailure(Call<Answer> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(Act_Question.this, "Internal Server Error.!\nPlease Try Later.!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    // Parse response got from server
    private void parseResponse(Answer response) {
        try {
            final String status = response.getStatus();
            final String answer = response.getAnswer();
            final String answerImage = response.getAnswerImage();
            final String question = response.getQuestion();
            if (status.equals("success")) { // If answer found in database

                et_question.setText("");
                Intent intent = new Intent(Act_Question.this, Act_Answer.class);            //this will send the questions,answer,imag it has received from server to Act_Answer
                startActivity(intent);
                intent.putExtra("question", strQuestion_user);                               //````````````````````chagess made -- question was replaced by strQuestion_user`````````````````
                intent.putExtra("answer", answer);
                intent.putExtra("answerImage", answerImage);
                //startActivity(intent);                                                      //it will start the next activity i.e after Act_Question -> Act_Answer

            } else if (status.equals("failure")) { // If answer not found in database
                Toast.makeText(Act_Question.this, answer, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(Act_Question.this, "Internal Server Error..\nPlease Try Again.!", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        //intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    et_question.setText(result.get(0));
                    img_send.performClick();
                }
                break;
            }

        }
    }

    // On Back Button pressed
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 1000);
    }
}
