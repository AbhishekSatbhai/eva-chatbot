package com.example.asus.aibot;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;

import java.io.IOException;
import java.util.List;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback
{
    private GoogleMap mMap;
    String flag;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Bundle extras = getIntent().getExtras();                                //import the flag from ACT_Answer
        if (extras != null) {
            flag = extras.getString("flag", "");
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        Toast.makeText(this, flag, Toast.LENGTH_SHORT).show();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

        @Override
        public void onMapReady (GoogleMap googleMap)
        {
            mMap = googleMap;

            //    Polygon polygon = mMap.addPolygon(new PolygonOptions().add(new LatLng(-35.016, 143.321),new LatLng(-34.747, 145.592),new LatLng(-34.364, 147.891),new LatLng(-33.501, 150.217),new LatLng(-32.306, 149.248),new LatLng(-32.491, 147.309)).strokeColor(Color.RED).fillColor(Color.BLUE));
            //   polygon.setTag("A");              //set tag to polygon


            // Add a marker for atm
            if (flag.equals("atm")) {
                //icici bank
                LatLng icici = new LatLng(20.014639, 73.821145);
                mMap.addMarker(new MarkerOptions().position(icici).title("ICICI bank"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(icici,15.2f));

                //hdfc bank
                LatLng hdfc = new LatLng(20.014562, 73.820788);
                mMap.addMarker(new MarkerOptions().position(hdfc).title("HDFC bank"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(hdfc));

                //bank of india
                LatLng boi = new LatLng(20.014523, 73.821027);
                mMap.addMarker(new MarkerOptions().position(boi).title("BOI bank"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(boi));

                //icici bank 2
                LatLng icici2 = new LatLng(20.013870, 73.810459);
                mMap.addMarker(new MarkerOptions().position(icici2).title("ICICI bank"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(icici2));

                //axis bank
                LatLng axis = new LatLng(20.014513, 73.820793);
                mMap.addMarker(new MarkerOptions().position(axis).title("AXIS bank"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(axis));

                //bank of india 2
                LatLng boi2 = new LatLng(20.012218, 73.8145115);
                mMap.addMarker(new MarkerOptions().position(boi2).title("BOI bank"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(boi2));

                //state bank of india
                LatLng sbi = new LatLng(20.017725, 73.826096);
                mMap.addMarker(new MarkerOptions().position(sbi).title("SBI bank"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sbi));

                //state bank of india
                LatLng sbi2 = new LatLng(20.018484, 73.815387);
                mMap.addMarker(new MarkerOptions().position(sbi2).title("SBI bank"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sbi2));

            }
            if (flag.equals("fastfood")) {
                //freindship
                LatLng friendship = new LatLng(20.014639, 73.821145);
                mMap.addMarker(new MarkerOptions().position(friendship).title("Friendship "));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(friendship,15.2f));

                //ATKT
                LatLng ATKT = new LatLng(20.014441, 73.820620);
                mMap.addMarker(new MarkerOptions().position(ATKT).title("ATKT "));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(ATKT));

                //Cafeatkt
                LatLng Cafeatkt = new LatLng(20.014366, 73.820568);
                mMap.addMarker(new MarkerOptions().position(Cafeatkt).title("Cafe ATKT "));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Cafeatkt));

                //Fork n Spoon
                LatLng forknspoon = new LatLng(20.014312, 73.820371);
                mMap.addMarker(new MarkerOptions().position(forknspoon).title("Fork n Spoon "));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(forknspoon));

                //Vicky's Family Restaurant
                LatLng vicky = new LatLng(20.015282, 73.82542);
                mMap.addMarker(new MarkerOptions().position(vicky).title("Vicky's Family restaurant "));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(vicky));

                //Cafe Creme
                LatLng creme = new LatLng(20.014174, 73.825643);
                mMap.addMarker(new MarkerOptions().position(creme).title("Cafe Creme "));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(creme));

                //Little Punjab
                LatLng littlepunj = new LatLng(20.016612, 73.824770);
                mMap.addMarker(new MarkerOptions().position(littlepunj).title("Little Punjab"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(littlepunj));

                //Tea Post Cafe
                LatLng teapost = new LatLng(20.014429, 73.820751);
                mMap.addMarker(new MarkerOptions().position(teapost).title("Tea Post Cafe"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(teapost));

                //Sai Krupa Snacks Point
                LatLng saikrupa = new LatLng(20.016612, 73.824770);
                mMap.addMarker(new MarkerOptions().position(saikrupa).title("Sai Krupa Snacks Point"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(saikrupa));

                //Vijus hotel
                LatLng vijus = new LatLng(20.014528, 73.820682);
                mMap.addMarker(new MarkerOptions().position(vijus).title("Vijus Hotel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(vijus));

                //Jayesh hotel
                LatLng jayesh = new LatLng(20.010917, 73.811283);
                mMap.addMarker(new MarkerOptions().position(jayesh).title("Jayesh Hotel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(jayesh));

                //Kunal hotel
                LatLng kunal = new LatLng(20.018887, 73.829190);
                mMap.addMarker(new MarkerOptions().position(kunal).title("Kunal Hotel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(kunal));

                //Bhagyashree Maratha Khanawal
                LatLng khanawal = new LatLng(20.017795, 73.829190);
                mMap.addMarker(new MarkerOptions().position(khanawal).title("Bhagyashree Maratha Hotel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(khanawal));

            }

            if (flag.equals("mess")) {
                //Pashupati mess
                LatLng pashupati = new LatLng(20.013285,73.825522);
                mMap.addMarker(new MarkerOptions().position(pashupati).title("Pashupati Mess"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pashupati,16.2f));

                //Moraya Lunch home
                LatLng moraya = new LatLng(20.012520,73.826572);
                mMap.addMarker(new MarkerOptions().position(moraya).title("Moraya Lunch home"));
               // mMap.moveCamera(CameraUpdateFactory.newLatLng(moraya));

                //Gani's mess and home
                LatLng Gani = new LatLng(20.018592, 73.827864);
                mMap.addMarker(new MarkerOptions().position(Gani).title("Gani's mess and home"));
               // mMap.moveCamera(CameraUpdateFactory.newLatLng(Gani));

                //Mahavir mess and restaurant
                LatLng Mahavir = new LatLng(20.011526, 73.823659);
                mMap.addMarker(new MarkerOptions().position(Mahavir).title("Mahavir mess and restaurant"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Mahavir,16.2f));                                                   //```````Changes here````````````````
                //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(20.011526, 73.823659), 4));
            }
            if (flag.equals("emergency")) {
                //Swami Narayan Police Station
                LatLng Swami = new LatLng(20.010565, 73.810136);
                mMap.addMarker(new MarkerOptions().position(Swami).title("Swami Narayan Police Station"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Swami,16.2f));

                //Adgaon Police Station
                LatLng Adgaon = new LatLng(20.021461, 73.842905);
                mMap.addMarker(new MarkerOptions().position(Adgaon).title("Adgaon Police Station"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Adgaon));


            }
            if (flag.equals("hostel")) {
                //V residency girls hostel
                LatLng Vresidency = new LatLng(20.012890, 73.824586);
                mMap.addMarker(new MarkerOptions().position(Vresidency).title("V residency girls hostel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Vresidency,15.2f));

                //Kanole Hostel
                LatLng Kanole = new LatLng(20.011946, 73.823131);
                mMap.addMarker(new MarkerOptions().position(Kanole).title("Kanole Hostel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Kanole));

                //Shivam Boys Hostel
                LatLng Shivam = new LatLng(20.011955, 73.823132);
                mMap.addMarker(new MarkerOptions().position(Shivam).title("Shivam Boys Hostel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Shivam));

                //Gajanan Hostel
                LatLng Gajanan = new LatLng(20.011779, 73.823708);
                mMap.addMarker(new MarkerOptions().position(Gajanan).title("Gajanan Hostel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Gajanan));

                //Modekeshwar Hostel
                LatLng Modekeshwar = new LatLng(20.014392, 73.824055);
                mMap.addMarker(new MarkerOptions().position(Modekeshwar).title("Modekeshwar Hostel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Modekeshwar));

                //Mitra Hostel
                LatLng Mitra = new LatLng(20.014470, 73.824230);
                mMap.addMarker(new MarkerOptions().position(Mitra).title("Mitra Hostel"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Mitra));


            }

            if (flag.equals("medical")) {
                //Shri Dutt Medical
                LatLng Shri = new LatLng(20.018852, 73.829933);
                mMap.addMarker(new MarkerOptions().position(Shri).title("Shri Dutt Medical"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Shri,16.2f));

                //Sumangal Medical
                LatLng Sumangal = new LatLng(20.015435, 73.825802);
                mMap.addMarker(new MarkerOptions().position(Sumangal).title("Sumangal Medical"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Sumangal));

                //Sai Shraddha Medical
                LatLng Shraddha = new LatLng(20.014826, 73.826710);
                mMap.addMarker(new MarkerOptions().position(Shraddha).title("Sai Shraddha Medical"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Shraddha));

                //Sunam Multi accident hospital
                LatLng Sunam = new LatLng(20.010967, 73.811376);
                mMap.addMarker(new MarkerOptions().position(Sunam).title("Sunam Multi accident hospital"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Sunam));

                //Apollo Hospital
                LatLng Apollo = new LatLng(20.012203, 73.816098);
                mMap.addMarker(new MarkerOptions().position(Apollo).title("Apollo Hospital"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Apollo));

                //Mauli Hospital
                LatLng Mauli = new LatLng(20.017444, 73.824679);
                mMap.addMarker(new MarkerOptions().position(Mauli).title("Mauli Hospital"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Mauli));

                //Dr. Parnerkar Hospital
                LatLng Parnerkar = new LatLng(20.017904, 73.829349);
                mMap.addMarker(new MarkerOptions().position(Parnerkar).title("Dr. Parnerkar Hospital"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Parnerkar));


            }

        }
    }